﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States
{
	class InstructionState: IStateBase
    {
        private StateManager manager;   // manager game class type

        /// <summary>
        /// Create the state
        /// </summary>
        /// <returns></returns>
        public InstructionState(StateManager managerRef)    // constructor
        {
            manager = managerRef;   // store a reference to StateManager class

            // Load Scene03
            if (Application.loadedLevelName != "Scene03")
            {
                Application.LoadLevel("Scene03");
            }
        }

        /// <summary>
        /// Update is called by stateManager once per frame when state active
        /// </summary>
        /// <returns></returns>
        public void StateUpdate()
        {
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        /// <returns></returns>
        public void ShowIt()
        {
            // draw the texture store in instructionStateSplash variable that is the size of the screen
            GUI.DrawTexture(new Rect(Screen.width/2 - 256 / 2, Screen.height/2 - 128 /2, 256, 128), manager.gameDataRef.instructionStateSplash, ScaleMode.StretchToFill);

            GUI.Box(new Rect(Screen.width / 2 - 160 / 2, Screen.height / 2, 160, 120), "Instruction state");

            // if button pressed or R keys
            if (GUI.Button(new Rect(10, 10, 250, 60), "Press Here or Any R to Return") || Input.GetKeyUp(KeyCode.R) )
            {
                // switch the state to Setup state
                manager.SwitchState(new SetupState(manager));
            }
            Debug.Log("In InstructionState");
        }
    }
}